#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

const int length_reader = 10;

struct Reader
{
	int id;
	char fullname[40];
	int old;
	int count_month;
	long pay_year;
};

Reader readers[length_reader];
int index_payyear_min = 0;
int index_payyear_max = 0;

void PrintLine()
{
	printf("\n------------------------------\n\n");
}
void ReadInput()
{
	int i;
	char temp[40];
	for (i = 0; i < length_reader; i++)
	{
		printf("Nhap thong tin doc gia thu %d \n", i + 1);

		printf("?Ma so: ");
		gets(temp);
		readers[i].id = atoi(temp);

		printf("?Ho va ten: ");
		gets(readers[i].fullname);

		printf("?Tuoi: ");
		gets(temp);
		readers[i].old = atoi(temp);

		printf("?So thang dang ky: ");
		gets(temp);
		readers[i].count_month = atoi(temp);

		readers[i].pay_year = 100000 * atoi(temp);

		PrintLine();
	}
}

void PrintReader(int i, bool isShowHeader)
{
	if (isShowHeader)
	{
		printf("%-10s %-40s %-10s %-20s %-10s\n", "Ma so", "Ho va ten", "Tuoi", "So thang/nam", "Tong tien/nam");
		int j;
		
		for(j=0;j<(10+40+10+20+10+8);j++)
		{
			printf("-");
		}	
		printf("\n");

	}
	printf("%-10d %-40s %-10d %-20d %-10d \n", readers[i].id, readers[i].fullname, readers[i].old, readers[i].count_month, readers[i].pay_year);
	
}
void PrintReaders()
{
	int i;

	for (i = 0; i < length_reader; i++)
	{
		PrintReader(i, i == 0);
	}
	printf("\n\n\n");
}

void FindPayYearMinMax()
{
	int i;
	long payyear_max = readers[0].pay_year;
	long payyear_min = readers[0].pay_year;
	for (i = 1; i < length_reader; i++)
	{
		if (readers[i].pay_year > payyear_max)
		{
			index_payyear_max = i;
			payyear_max = readers[i].pay_year;
		}
		
		if(readers[i].pay_year < payyear_min)
		{
			index_payyear_min = i;
			payyear_min = readers[i].pay_year;
		}
	}
}
long TotalPayYear()
{
	int i;
	long res = 0;
	for (i = 0; i < length_reader; i++)
	{
		res += readers[i].pay_year;
	}
	return res;
}

void OrderByPayYear(bool isInc)
{
	int c, d;
	Reader swap;
	for (c = 0; c < length_reader - 1; c++)
	{
		for (d = 0; d < length_reader - c - 1; d++)
		{
			bool conditionCheck = isInc ? readers[d].pay_year > readers[d + 1].pay_year : readers[d].pay_year < readers[d + 1].pay_year;

			if (conditionCheck)
			{
				swap = readers[d];
				readers[d] = readers[d + 1];
				readers[d + 1] = swap;
			}
		}
	}
}
void OrderByPayYearDesc()
{
	OrderByPayYear(false);
}
void OrderByPayYearInc()
{
	OrderByPayYear(true);
}
int main()
{
	ReadInput();
	PrintReaders();
	PrintLine();

	//
	FindPayYearMinMax();

	printf("Doc gia tra tien cao nhat\n");
	PrintReader(index_payyear_max, true);
	printf("\n\n\n");


	printf("Doc gia tra tien thap nhat\n");
	PrintReader(index_payyear_min, true);
	printf("\n\n\n");


	printf("Tong tien thu vien nhan duoc: %d\n", TotalPayYear());
	printf("\n\n\n");


	//
	printf("Sap xep tong tien tang dan \n");
	OrderByPayYearInc();
	PrintReaders();
	//PrintLine();

	//
	printf("Sap xep tong tien giam dan \n");
	OrderByPayYearDesc();
	PrintReaders();
	//PrintLine();

	return 0;
}

