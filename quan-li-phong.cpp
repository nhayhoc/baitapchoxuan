#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>

const int MAX_ROOM = 10;
const int MAX_RENTED_ROOM = 20;
struct Room
{
	int id;
	char name[40];
	int count;
	int valid_count;
	int price;
	long total_price;
};
struct RentedRoom {
	int room_id;
	char room_name[40];
	int night_count;
	int price;
};
Room rooms[MAX_ROOM];
RentedRoom rented_rooms[MAX_RENTED_ROOM];
int rooms_length;
int rented_room_nextindex = 0;
long total_price = 0;

void PrintHeaderTable(int length){
	int j;
	for(j=0;j<length;j++)
	{
		printf("-");
	}	
	printf("\n");
}

void ReadInput()
{
	printf("\n\nChon 1. Nhap thong tin phong\n");
	int i;
	char temp[40];
	printf("?So loai phong: ");
	gets(temp);
	rooms_length = atoi(temp);
	
	
	for (i = 0; i < rooms_length; i++)
	{
		printf("Nhap thong tin phong thu %d \n", i + 1);

		printf("?Ma phong: ");
		gets(temp);
		rooms[i].id = atoi(temp);

		printf("?Ten phong: ");
		gets(rooms[i].name);

		printf("?Gia tien: ");
		gets(temp);
		rooms[i].price = atoi(temp);

		printf("?So phong: ");
		gets(temp);
		rooms[i].count = atoi(temp);
		rooms[i].valid_count = atoi(temp);
		
		rooms[i].total_price = rooms[i].price * rooms[i].count ;
		total_price += rooms[i].total_price ;
		
		printf("\n------------------------------\n\n");
	}
}

void PrintRooms(int type){
	int i;
	printf("%-10s %-40s %-20s %-20s\n", "Ma so", "Ten phong", "Gia tien/dem", type==1?"So phong trong":"So phong da cho thue");
	PrintHeaderTable(10+40+20+20+8);
	for (i = 0; i < rooms_length; i++)
	{
		int rented = rooms[i].count - rooms[i].valid_count;
		if((type==1 && rooms[i].valid_count>0) || (type==2 && rented> 0)){
			printf("%-10d %-40s %-20d %-20d \n", rooms[i].id, rooms[i].name, rooms[i].price, type==1?rooms[i].valid_count:rented);
		}
	}
	printf("\n\n\n");
}
void PrintRoomsValid()
{
	PrintRooms(1);
}
void PrintRoomsRented()
{
	PrintRooms(2);
}

void RegisterRoom(){
	//1. tim room
	//2. xem co phong trong ko
	//3. giam valid_count
	//4. them vao bang rentedroom
	
	PrintRoomsValid();
	
	
	char temp[40];
	int id;
	int night_count;
	printf("Thue phong\n");
	
	printf("\t?Ma so phong: ");
	gets(temp);
	id = atoi(temp);
	
	printf("\t?So dem: ");
	gets(temp);
	night_count = atoi(temp);	
		
		
		
	int i;
	int isSuccess = false;
	for (i = 0; i < rooms_length; i++)
	{
		if(rooms[i].id == id && rooms[i].valid_count>0) {
			rooms[i].valid_count--;
			
			rented_rooms[rented_room_nextindex].room_id = rooms[i].id;
			strcpy(rented_rooms[rented_room_nextindex].room_name,rooms[i].name);
			rented_rooms[rented_room_nextindex].night_count = night_count;
			rented_rooms[rented_room_nextindex].price = rooms[i].price * night_count;
			rented_room_nextindex++;
			
			isSuccess = true;
			break;
		}
	}
	if(isSuccess){
		printf("Cho thue thanh cong!\n");
	}
	else {
		printf("Cho thue that bai, vui long nhap thong tin hop le!\n");
	}
}

void PrintRentedDetail() {
	int i;
	printf("%-10s %-40s %-20s %-20s\n", "STT", "Ten phong", "So dem", "So tien");
	PrintHeaderTable(10+40+20+20+8);
	for (i = 0; i < rented_room_nextindex; i++)
	{
		printf("%-10d %-40s %-20d %-20d \n",i, rented_rooms[i].room_name,rented_rooms[i].night_count,rented_rooms[i].price  );
	}
	printf("\n\n\n");
}

void PaymentRoom() {
	//1. xoa o bang cho thue
	//2. tim room
	//3. tang valid_count o bang phong
	
	char temp[40];
	int index_rented_room;
	
	PrintRentedDetail();
	printf("Tra phong\n");
	
	printf("\t?Tra phong so: ");
	gets(temp);
	int remove_index = atoi(temp);
	
	if(remove_index >= 0 && remove_index<rented_room_nextindex) {
		//remove this element from rented_rooms, a copy code o day nay https://www.programmingsimplified.com/c/source-code/c-program-delete-element-from-array
		int c;
		for (c = remove_index; c < rented_room_nextindex - 2; c++)
	        rented_rooms[c] = rented_rooms[c+1];
		
		for(c=0;c < rooms_length; c++) {
			if(rooms[c].id == rented_rooms[remove_index].room_id) {
				rooms[c].valid_count++;
				printf("Tra phong thanh cong!\n");
				break;
			}
		}
	}
	else {
			printf("Vui long nhap thong tin hop le\n");
	}
	
}
void Total(){
	total_price= 0;
	int i ;
	for(i = 0;i< rented_room_nextindex; i ++)
		total_price += rented_rooms[i].	price;
}

int main()
{
	while(true) {
		int choose;
		char temp[40];
		printf("Menu\n");
		printf("\t 1. Nhap du lieu\n");
		printf("\t 2. Dang ki phong\n");
		printf("\t 3. Tinh tong tien phong phai tra\n");
		printf("\t 4. Danh sach phong dang cho thue\n");
		printf("\t 5. Tra phong\n");
		printf("\t 6. Danh sach phong trong\n");
		
		printf("?Lua chon: ");
		gets(temp);
		choose = atoi(temp);
		switch (choose)
		{
		    case 1: 
		    	ReadInput();
		        break;
		    case 2: 
		    	RegisterRoom();
		        break;
		    case 3: 
		    	Total();
		    	printf("Tong tien: %d\n", total_price);
		        break;
		    case 4: 
		    	PrintRoomsRented();
		        break;
		    case 5: 
		    	PaymentRoom();
		        break;
		    case 6: 
		    	PrintRoomsValid();
		        break;
		    default: // code to be executed if n doesn't match any cases
		    	break;
		}
	}

	return 0;
}

